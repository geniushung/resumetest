package com.jimmy.resumetest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.jimmy.resumetest.adapter.DataListAdapter
import com.jimmy.resumetest.data.ListData
import com.jimmy.resumetest.databinding.ActivityMainBinding
import com.jimmy.resumetest.presenter.ApiInterface
import com.jimmy.resumetest.presenter.ApiPresenter

class MainActivity : AppCompatActivity(), ApiInterface {

    private var adapter : DataListAdapter? = null
    private var presenter: ApiPresenter? = null
    private lateinit var binding : ActivityMainBinding

    fun log(msg : String) {
        Log.i("jimmy", msg)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onResume() {
        super.onResume()
        presenter = ApiPresenter()
        presenter?.attach(this)
        presenter?.getData()

        adapter = DataListAdapter(this)

        binding.rcvDataList.adapter = adapter
    }

    override fun getData(response: List<ListData>) {
        log("get Data")
        adapter?.setDataArray(response)
    }
}