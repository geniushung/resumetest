package com.jimmy.resumetest.base

import com.jimmy.resumetest.api.ApiService

abstract class BasePresenter<T> {
    @JvmField
    var mView: T? = null
    @JvmField
    protected var api = ApiService.getApi()

    fun attach(mView: T) {
        this.mView = mView
    }

    fun detach() {
        mView = null
    }
}