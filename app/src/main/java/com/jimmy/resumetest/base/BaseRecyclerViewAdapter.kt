package com.jimmy.resumetest.base

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import java.util.*

/**
 * Created by genius on 2019/10/23.
 */
abstract class BaseRecyclerViewAdapter<T>(baseContext : Context)
    : Adapter<ViewHolder>() {

    private var mArray = ArrayList<Any?>()

    var mInflater: LayoutInflater = baseContext.getSystemService(
        Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    protected abstract fun onCreateMyViewHolder(
        parent: ViewGroup?, viewType: Int) : ViewHolder

    protected abstract fun onBindMyViewHolder(
        holder: ViewHolder?, position: Int)

    @SuppressLint("NotifyDataSetChanged")
    fun setDataArray(ary: Any?) {

        ary?.let {
            mArray.clear()
            mArray.addAll(it as ArrayList<*>)
            notifyDataSetChanged()
        }
    }

    fun updateDataArray(ary: Any?) {
        ary?.let {
            mArray.clear()
            mArray.addAll(it as ArrayList<*>)
        }
    }

    fun getDataArray() : ArrayList<Any?> {
        return mArray
    }

    override fun getItemCount(): Int {
        return mArray.size
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int) : ViewHolder {

        return onCreateMyViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        onBindMyViewHolder(holder, position)
    }
}