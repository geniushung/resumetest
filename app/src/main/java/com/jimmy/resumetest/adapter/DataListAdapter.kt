package com.jimmy.resumetest.adapter

import android.content.Context
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jimmy.resumetest.R
import com.jimmy.resumetest.base.BaseRecyclerViewAdapter
import com.jimmy.resumetest.data.ListData
import com.jimmy.resumetest.databinding.CellDataListItemBinding
import com.jimmy.resumetest.utilities.Utility

class DataListAdapter(context: Context) : BaseRecyclerViewAdapter<RecyclerView.ViewHolder>(context) {

    val ct = context

    override fun onCreateMyViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        val binding = CellDataListItemBinding.inflate(mInflater)
        return CellDataListIteViewHolder(binding, context = ct)
    }

    override fun onBindMyViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        val data = getDataArray()[position] as ListData
        val viewHolder = holder as CellDataListIteViewHolder
        viewHolder.setData(data)
    }

    class CellDataListIteViewHolder(binding : CellDataListItemBinding, context : Context) : RecyclerView.ViewHolder(binding.root) {

        private val mBinding = binding
        private var ct = context

        fun setData(data : ListData) {

            val time = Utility.getTimeString(data.T, "hh:mm:ss")
            val price = data.p.subSequence(0, data.p.length - 4)
            val volume = data.q.subSequence(0, data.p.length - 4)

            mBinding.txtTime.text = time
            mBinding.cvPrice.text = price
            mBinding.txtVolume.text = volume

            if(data.m)
                mBinding.cvPrice.setTextColor(ContextCompat.getColor(ct, R.color.colorPriceRed))
            else
                mBinding.cvPrice.setTextColor(ContextCompat.getColor(ct, R.color.colorPriceGreen))

        }
    }
}