package com.jimmy.resumetest.data

/*
*
* [
  {
    "a": 26129,         // 归集成交ID
    "p": "0.01633102",  // 成交价
    "q": "4.70443515",  // 成交量
    "f": 27781,         // 被归集的首个成交ID
    "l": 27781,         // 被归集的末个成交ID
    "T": 1498793709153, // 成交时间
    "m": true,          // 是否为主动卖出单
    "M": true           // 是否为最优撮合单(可忽略，目前总为最优撮合)
  }]

*
* */
data class ListData(
    val a: Int,
    val p: String,
    val q: String,
    val f: Long,
    val l: Long,
    val T: Long,
    val m : Boolean
)