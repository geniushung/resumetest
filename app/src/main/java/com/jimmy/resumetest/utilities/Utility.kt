package com.jimmy.resumetest.utilities

import java.text.SimpleDateFormat
import java.util.*

object Utility {

    fun getTimeString(millionSec : Long, dateFormat : String) : String {
        val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())
        val calendar: Calendar = Calendar.getInstance()
        calendar.timeInMillis = millionSec
        return formatter.format(calendar.time)
    }

}