package com.jimmy.resumetest.api;

import com.jimmy.resumetest.data.ListData;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface Api {

    @GET("/api/v1/aggTrades?symbol=BTCUSDT&limit=200")
    Observable<List<ListData>> getApiData();
}
