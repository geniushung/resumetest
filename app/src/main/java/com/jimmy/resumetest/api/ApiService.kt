package com.jimmy.resumetest.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception
import java.util.concurrent.TimeUnit

object ApiService {

    const val ApiDomain = "https://api.yshyqxx.com/"
    const val TimeOutSec = 15

    private var retrofit: Retrofit? = null
    private var api: Api? = null


    fun getInstance(): Retrofit? {

        try {
            if (retrofit == null) {

                val okHttpClientBuilder = OkHttpClient.Builder()

                val logging = HttpLoggingInterceptor()

                logging.setLevel(HttpLoggingInterceptor.Level.BODY)

                okHttpClientBuilder
                    .connectTimeout(
                        TimeOutSec.toLong(),
                        TimeUnit.SECONDS
                    )
                    .writeTimeout(
                        TimeOutSec.toLong(),
                        TimeUnit.SECONDS
                    )
                    .readTimeout(
                        TimeOutSec.toLong(),
                        TimeUnit.SECONDS
                    )

                okHttpClientBuilder.addInterceptor(logging)

                okHttpClientBuilder.hostnameVerifier(SSLSocketClient.hostnameVerifier)

                retrofit = Retrofit.Builder()
                    .baseUrl(ApiDomain)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClientBuilder.build())
                    .build()
            }
        } catch (e: Exception) {

        }
        return retrofit
    }

    fun getApi(): Api? {
        if (api == null) {
            api = getInstance()?.create(Api::class.java)
        }
        return api
    }
}