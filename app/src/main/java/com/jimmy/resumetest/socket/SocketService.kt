package com.jimmy.resumetest.socket

import android.util.Log
import org.json.JSONObject
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.lang.Exception
import java.net.InetAddress
import java.net.Socket

class SocketService {

    //客戶端的socket
    private var clientSocket: Socket? = null
    //取得網路輸出串流
    private var bw: BufferedWriter? = null
    //取得網路輸入串流
    private var br: BufferedReader? = null

    private var tmp: String? = null

    private val jsonWrite: JSONObject? = null
    private var jsonRead: JSONObject? = null

    private val Connection = Runnable {
        // TODO Auto-generated method stub
        try {
            //輸入 Server 端的 IP
            val serverIp: InetAddress = InetAddress.getByName("wss://stream.yshyqxx.com")
            //自訂所使用的 Port(1024 ~ 65535)
            val serverPort = 5050
            //建立連線
            clientSocket = Socket(serverIp, serverPort)
            //取得網路輸出串流
            bw = BufferedWriter(OutputStreamWriter(clientSocket?.getOutputStream()))
            //取得網路輸入串流
            br = BufferedReader(InputStreamReader(clientSocket?.getInputStream()))

            //檢查是否已連線
            while (clientSocket?.isConnected() == true) {
                //宣告一個緩衝,從br串流讀取 Server 端傳來的訊息
                tmp = br?.readLine()
                if (tmp != null) {
                    //將取到的String抓取{}範圍資料
                    tmp = tmp!!.substring(tmp!!.indexOf("{"), tmp!!.lastIndexOf("}") + 1)
                    jsonRead = JSONObject(tmp!!)
                    //從java伺服器取得值後做拆解,可使用switch做不同動作的處理
                }
            }
        } catch (e: Exception) {
            //當斷線時會跳到 catch,可以在這裡處理斷開連線後的邏輯
            e.printStackTrace()
            Log.e("text", "Socket連線=$e")
            //finish() //當斷線時自動關閉 Socket
        }
    }
}