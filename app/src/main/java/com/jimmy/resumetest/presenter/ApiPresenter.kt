package com.jimmy.resumetest.presenter

import android.util.Log
import com.jimmy.resumetest.api.Api
import com.jimmy.resumetest.base.BasePresenter
import com.jimmy.resumetest.data.ListData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers

class ApiPresenter : BasePresenter<ApiInterface?>() {

    fun getData() {

        api?.apiData?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeOn(Schedulers.io())
            ?.subscribe(object : DefaultObserver<List<ListData>>() {

                override fun onNext(response: List<ListData>) {
                    mView?.getData(response)
                }

                override fun onError(e: Throwable) {
                    Log.i("jimmy", e.localizedMessage ?: "unknown error")
                }

                override fun onComplete() {
                    Log.i("jimmy", "onComplete")
                }
            })
    }

}