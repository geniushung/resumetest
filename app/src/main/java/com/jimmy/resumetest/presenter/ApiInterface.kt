package com.jimmy.resumetest.presenter

import com.jimmy.resumetest.data.ListData

interface ApiInterface {
    fun getData(response: List<ListData>)
}